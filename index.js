const App = require('express')()

App.use(require('body-parser').json({ limit: '2mb' }))

App.get('/', (req, res) => res.json({ path: 'index' }))
App.get('/test', (req, res) => res.json({ path: 'test' }))
App.post('/', (req, res) => res.json({ path: 'post', body: req.body }))

App.listen(process.env.PORT || 3000, () => {
    console.log('server ok')
})
